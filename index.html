<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=1024" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>OpenMC|Open-source Monte Carlo particle transport code</title>

    <meta name="OpenMC" />
    <meta name="author" content="Bryan Herman" />

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:regular,semibold,italic,italicsemibold|PT+Sans:400,700,400italic,700italic|PT+Serif:400,700,400italic,700italic" rel="stylesheet" />
    <link href="css/openmc.css" rel="stylesheet" />
    <link rel="shortcut icon" href="favicon.png" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
    </script>
    <script type="text/javascript" async
      src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML">
    </script>
    <script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
    </script>
</head>

<body class="impress-not-supported">

<div class="fallback-message">
    <p>Your browser <b>doesn't support the features required</b> by impress.js,
       so you are presented with a simplified version of this presentation.</p>
    <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
</div>

<div id="impress">

<!-- ======================================================================= -->
    <div id="title" class="step slide" data-x="0" data-y="0">
        <img src="images/openmc_logo.svg" alt="OpenMC logo" width="900px">
        <q class="center">Monte Carlo particle transport code </q>
        <q class="center"><strong>Bryan Herman</strong></q>
        <img class="float-left" src="images/MIT_logo.svg" alt="MIT logo" width="100px">
        <img class="float-right" src="images/crpg.png" alt="CRPG logo" width="200px">
    </div>
<!-- ======================================================================= -->
    <div id="goals" class="step slide" data-x="200" data-y="-1000"
        data-rotate="15">
        <h1>OpenMC Project Goals</h1>
        <ul>
            <li> Develop a Monte Carlo framework for reactor analysis
                 that scales on leadership-class supercomputers
                 (100,000+ cores).
            </li>
            <li> Provide a simple tool to analyze performance and
                 limitations on proposed architectures.
            </li>
            <li> Objectives:
            <ul>
                 <li> Realistic physics
                 <li> Modern programming style and data structures </li>
                 <li> Extensible for research purposes </li>
                 <li> Open source and freely available </li>
             </ul>
            </li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="features" class="step slide" data-x="600" data-y="-2000"
        data-rotate="30">
        <h1>List of Features</h1>
        <div class="frame right half">
            <h1>Current features</h1>
            <ul>
                <li> Fixed source and k-eigenvalue calculations </li>
                <li> <strong>Geometry</strong>: CSG with second
                     order surfaces, universes,
                     rotations/translations,
                     rectangular/hexagonal lattices </li>
                <li> <strong>Cross Sections</strong>: ACE
                     formal or multi-group </li>
                <li> <strong>Physics</strong>: neutron transport,
                     S(a,b) thermal scattering, probability tables,
                     target thermal motion </li>
                <li> Flexible general tally system </li>
                <li> <strong>Parallelism</strong>:
                     Distributed/shared-memory via MPI/OpenMP </li>
                <li> Rich, extensible Python API </li>
                <li> <strong>Input</strong>: XML or Python
                     API-driven </li>
                <li> <strong>Output</strong>: HDF5</li>
                <li> Multi-group cross section generation </li>
            </ul>
        </div>
        <div class="frame right half">
            <h1>Short-term</h1>
            <ul>
                <li> HDF5 formal cross sections </li>
                <li> Photon transport </li>
                <li> Depletion </li>
                <li> Transient-capability </li>
                <li> Temperature dependence </li>
                <li> Domain decomposition </li>
            <ul>
        </div>
        <div class="frame right half">
            <h1>Long-term</h1>
            <ul>
                <li> In-house nuclear data processing </li>
                <li> Thermal-hydraulics coupling </li>
                <li> Non-CSG deformable geometry </li>
                <li> Coupling with MOOSE </li>
            <ul>
        </div>
    </div>
<!-- ======================================================================= -->
    <div id="validation" class="step slide" data-x="1300" data-y="-3000"
        data-rotate="45">
        <h1>Verification and Validation</h1>
        <p>
            <img src="images/expanded-suite.svg" alt="Verification Suite" width="1100px" align="middle" />
        </p>
    </div>
<!-- ======================================================================= -->
    <div id="parallel" class="step slide" data-x="2200" data-y="-3600"
        data-rotate="60">
        <h1>Parallel Performance</h1>
        <img class="float-right" src="images/scaling_loglog.svg" width="600px" />
        <ul><li>MC Performance Benchmark</li></ul>
        <img class="float-left padded" src="images/mcperformance.svg" width="400px" />
        <ul>
            <li>Simulation of Mira Supercomputer</li>
            <li>49,152 nodes, 786,432 cores</li>
            <li>4 hw threads/core = 3,145,728 threads</li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="atr" class="step slide" data-x="3200" data-y="-4000"
        data-rotate="75">
        <h1>Advanced Geometry Modeling</h1>
        <p>
            <img src="images/atr.png" width="500px" />
        </p>
    </div>
<!-- ======================================================================= -->
    <div id="geometry" class="step slide" data-x="4200" data-y="-4100"
        data-rotate="90">
        <h1>Constructive Solid Geometry</h1>
        <ul>
            <li> A Cell is defined by assigning a material or
            fill to a region.</li>
            <li> A Region is defined as intersection, union,
            and/or complement surface half-spaces.</li>
            <li> A Surface is a locus of zeros of a function
            <ul>
                <li> Planes, e.g., $x - x_0 = 0$</li>
                <li> Parallel cylinders, e.g., $y^2 - z^2 -R^2 = 0$</li>
                <li> Sphere, e.g., $x^2 + y^2 + z^2 -R^2 = 0$</li>
                <li> Parallel Cones, e.g., $x^2 + z^2 - R^2y^2 = 0$</li>
                <li> General quadratic </li>
            </ul>
            </li>
            <li>A surface half-space is the region whose points satisfy a
            positive/negative inequality of the surface equations</li>
            <li>Universes, rectangular and hexagonal lattices
            and rotation/translation.</li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="beavrs" class="step slide" data-x="5200" data-y="-3950"
        data-rotate="105">
        <h1>BEAVRS Benchmark</h1>
        <img class="float-left" src="images/pwr_core.png" width="500px" />
        <img class="float-right" src="images/pwr_axial.png" width="500px" />
    </div>
<!-- ======================================================================= -->
<div id="beavrs-val" class="step slide" data-x="6150" data-y="-3500"
    data-rotate="120">
    <img class="float-left" src="images/detector.png" width="500px" />
    <img class="float-right" src="images/all_splines.svg" width="450px" />
    <img class="float-right" src="images/boron_letdown.svg" width="450px" />
</div>
<!-- ======================================================================= -->
<div id="eig-probs" class="step slide" data-x="6900" data-y="-2900"
    data-rotate="135">
    <h1>Eigenvalue Problems</h1>
    <ul><li>Reaction rate balance for infinite medium:</li></ul>
    <div class="center"> Absorption = Fission Production</div>
    $$\Sigma_a\phi = \frac{1}{k}\nu\Sigma_f\phi$$
    <ul><li>An eigenvalue problem - need to guess the flux and eigenvalue
            until both sides of the equation balance.</li>
        <li>These are referred to as fission source iterations in a Monte
            Carlo simulation.</li>
        <li>Results cannot be statistically accumulated until fission source
            is converged.</li>
        <li>Can be accelerated using diffusion theory.</li>
    </ul>
</div>
<!-- ======================================================================= -->
    <div id="cmfd" class="step slide" data-x="7500" data-y="-2000"
        data-rotate="150">
        <h1>Coarse Mesh Diffusion</h1>
        <div class="frame overlay">
            <ul>
                <li> Shannon entropy is a metric for assessing
                    source convergence. </li>
                <li> When it becomes stationary, source is converged. </li>
                <li> Without Coarse Mesh Finite Difference (CMFD), simulation
                    took approx. <blue>200 iterations</blue> (batches).</li>
                <li> With CMFD simulation took approx. <red>25 iterations</red>.</li>
            </ul>
        </div>
        <p>
            <img src="images/entropy_3D.svg" width="900px" />
        </p>
    </div>
<!-- ======================================================================= -->
<div id="tallies" class="step slide" data-x="7900" data-y="-1000"
    data-rotate="165">
    <h1>Tally System</h1>
    <h2> Tallies are defined as combinations of filters ans scores </h2>
    $$ X = \underbrace{\int d^3r \int d\Omega \int dE}_{\rm filters}
    \underbrace{f(\vec{r},\widehat\Omega,E)}_{\rm scores}\psi(\vec{r},\widehat\Omega,E) $$
    <ul>
        <li> Filters: cell, material, energy, outgoing energy, mesh, ...</li>
        <li> Scores: flux, total, fission, nu-fission, scatter, absorption,
        scattering moments, individual reactions, ...</li>
        <li> Can also specify nuclides </li>
    </ul>
</div>
<!-- ======================================================================= -->
    <div id="software-arch" class="step slide" data-x="8000" data-y="0"
        data-rotate="180">
        <h1>Software Architecture</h1>
        <img class="float-right" src="images/sphinx.png" width="200px" />
        <ul>
            <li> ~40,000 lines of Fortran 2008 code </li>
            <li> Distributed-memory parallelism via MPI </li>
        <img class="float-right" src="images/git.png" width="200px" />
            <li> Shared-memory parallelism via OpenMP </li>
            <li> CMake build system for portability </li>
            <li> ~20,000 lines of Python API code </li>
            <li> Version control through Git </li>
        <img class="float-right" src="images/github.png" width="200px" />
            <li> Code hosting, bug tracking through Github
                <ul><li><a href="https://github.com/mit-crpg/openmc">
                    https://github.com/mit-crpg/openmc</a></li></ul>
            </li>
            <li> Automatic documentation generation with Sphinx
            <ul><li><a href="http://openmc.readthedocs.io">
                    http://openmc.readthedocs.io</a></li></ul>
            </li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="python-api" class="step slide" data-x="7900" data-y="900"
        data-rotate="195">
        <h1>Python API Input</h1>
        <div class="frame">
            <ul>
                <li> OpenMC includes a rich Python API that enables
                programmatic pre- and post-processing </li>
                <li> To construct input files (and analyze output)
                you, as the user, must write a Python script
                utilizing classes defined by the OpenMC API </li>
                <li> There are many benefits to using Python API
                over direct use of OpenMC XML, for example:
                <ul>
                    <li> All the usual benefits of Python -
                    variables vs hard-coded numbers, standard library,
                    third-party packages </li>
                    <li> Run-time type checking </li>
                    <li> Perturbation or sensitivity studies </li>
                    <li> Extra features in our API - data analysis,
                    multigroup cross section generation, nuclear
                    data parsers, etc. </li>
                </ul>
                </li>
            </ul>
        </div>
    </div>
<!-- ======================================================================= -->
    <div id="python-eco" class="step slide" data-x="7200" data-y="2000"
        data-rotate="215">
        <h1>Leverage the Python Ecosystem</h1>
        <img src=images/python_ecosystem.png width="800px" />
    </div>
<!-- ======================================================================= -->
    <div id="research" class="step slide" data-x="6500" data-y="2800"
        data-rotate="190">
        <h1>Extensible for Research</h1>

        <ul>
            <li>OpenMC was written so that anyone can implement their research
                algorithms.</li>
        </ul>

        <div class="frame">
            <h1> Some Research Topics in OpenMC </h1>
            <ul>
                <li> Parallel Algorithms </li>
                <li> Data Management </li>
                <li> Source Convergence Acceleration (CMFD, LOO) </li>
                <li> Domain Decomposition </li>
                <li> Multipole Representation of Cross Sections </li>
                <li> Doppler Broadening </li>
                <li> Multigroup Monte Carlo and Cross Section Generation </li>
                <li> Multiphysics Simulations </li>
                <li> Diffusion Coefficients </li>
                <li> Big Data </li>
                <li> <b> So many more we don't even know about! </b> </li>
            </ul>
        </div>
    </div>
<!-- ======================================================================= -->
    <div id="education" class="step slide" data-x="6000" data-y="3700"
        data-rotate="187">
        <h1>Educational Use</h1>
        <ul>
            <li> Great for use in a simulation lab </li>
            <li> Highly recommend for senior design projects </li>
        </ul>
         <!--   <img class='float-right' src=images/fission_mean.png
                width="300px" /> -->
        <img class='float-right' src=images/mgxs.png width="500px" />
        <img class='float-left' src=images/phi_het.png width="500px" />
        <strong>Heterogeneous flux reconstruction </strong>
    </div>
<!-- ======================================================================= -->
    <div id="visualization" class="step slide" data-x="5000" data-y="4800"
        data-rotate="180">
        <h1>Particle Track Visualization</h1>
        <ul><li>Visualization in Paraview</li>
        <li>Helpful for tracking errors</li></ul>
        <img src='images/Tracks.png' width='600px' />
    </div>
<!-- ======================================================================= -->
    <div id="futures" class="step slide" data-x="5000" data-y="5800">
        <h1>Upcoming Features</h1>
        <ul>
            <li> On-the-fly Doppler Broadening </li>
            <li> Domain Decomposition </li>
            <li> Multiphysics </li>
            <li> Differential Tallies </li>
            <li> Transient Analysis </li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="doppler" class="step slide" data-x="5000" data-y="6800">
        <h1>Doppler Broadening</h1>
        <img class="float-right" src="images/multipole_conversion.png" width="300px" />
        <ul>
            <li>On-the-fly Doppler broadening based on the windowed multipole
            method (300 of the 423 nuclides in ENDF/BVII.1)</li>
            <li>On-the-fly URR sampling or equiprobable E-T surfaces</li>
            <li>Kernel Reconstruction for remaining 100 nuclides</li>
            <li>An HDF5-formatted cross section library is being developed.</li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="domain" class="step slide" data-x="4300" data-y="9500">
        <h1>Domain Decomposition</h1>
        <img class="float-right" src="images/dd_beavrs.png" width="600px" />
        <ul>
            <li>Arbitrary mesh domain decomposition for distributing memory
            from tallies and material compositions</li>
            <li>Load balancing achieved by domain replication</li>
            <li>Still being reviewed</li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="multiphysics" class="step slide" data-x="5700" data-y="9500">
        <h1>Multiphysics</h1>
        <img class="float-right" src="images/coupling.png" width="600px" />
        <ul>
            <li>OpenMC was built as a library and incorporated as a MultiApp in
            Moose</li>
            <li>Functional expansion tallies were implemented to transfer data
            between CSG and FEM</li>
            <li>Continuous material tracking was implemented to allow for
            continuously varying temperature and nuclide densities within a CSG
            cell</li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="differential" class="step slide" data-x="5700" data-y="10500">
        <h1>Differential Tallies</h1>
        <img class="float-right" src="images/diff_tallies.png" width="480px" />
        <ul>
            <li>Implemented differential tallies to tally first order
            derivatives with density changes</li>
            <li>Extended to include temperature using first order derivatives
            of the multipole equations</li>
            <li>OpenMC will thus be able to provide accurate temperature and
            density reactivity coefficients in a single run</li>
            <li>Can also provide cross section temperature derivatives</li>
        </ul>
    </div>
<!-- ======================================================================= -->
    <div id="transient" class="step slide" data-x="4300" data-y="10500">
        <h1>Transient Analysis</h1>
        <ul><li>Implementing multigrid amplitude function (MAF) approach where
    the CMFD operator will be advanced on fine time steps with parameters from
    OpenMC on coarse time steps</li></ul>
        <img class="float-center" src="images/time_openmc.png" width="900px" />
    </div>
<!-- ======================================================================= -->

    <div id="all" class="step" data-x="3500" data-y="2700" data-scale="20">
    </div>

<script src="js/impress.js"></script>
<script>impress().init();</script>

</body>
</html>
